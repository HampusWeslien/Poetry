# The Crimson King

## Improvments

Why is greatness so hard to understand?


## Pitch:
A man's struggle against masculinity

## Themes:

Disney villains, rockstars, red, mansculinity,
merging of male/female, overarching narative

## Motives:
The Sirens' Tune, Cerberus, Hades, Eclipse, rockstar,

## Structure:

#### 1. How could I fall into your hands rosy maid?
**Story:** Introduces the character of the crimson king.

**Core:** the pain of having your work reinterpreted as you let other people consume it.

**Style:** broken shakspearean sonnet

#### Lady Fair
**Story:** Forbidden love, Rockstar muse

**Core:**
It's about falling in love with inspiration, and how ephemeral it can be.
It's possessive, like it can own her.

**Style:** Duality

#### the Grand Ball
**Story:** 
A bunch of ungrateful guests arive at the castle. 
The crimson king have created a rockstar for them
Who puts on a show to inspire all the poeple who control him.

**Core:** 
Ownership of art?
It ridicules the idea that you need inspiration.
It is not certain if the rockstar muse is male or female.
Refrences to both Bob Dylan and Jimi Hendrix.
The discription of the muse is describing the painting 'the birth of venus'.

**Style:** non-conforming

#### Ode to a Princess
**Story:** It doesn't have one, it's very dreamlike.

**Core:** 
The first stanza is about where ideas come from.
The second is reminising over ideas that hasn't been attampted yet, and sullied.
The third takes a turn, when the idea is 'burned to ash' all untreated mental issues come roaring.

**Style:** Ode


#### Little Ligth
**Story:** 
The king is on the run and the writter is being extorted.


**Core:** 
Creepy nursery rhyme.
It's about turning a jumbled mess of word into a unified whole

**Style:** Reformated *twinkle twinkl little star*

#### Middle Hades
**Story:**
Dream like stuff

**Core:**
About the creativity in art and how one can pour oneself into something and still noone cares.
And that other poeople matter more than art.
It is unclear wether the bride in the end is a person or the love for art.

**Style:**
Ballad

#### All along the Interlude
**Story:**
About the 4 horsemen descending on earth.

**Core:** 
The four horsemen represents different bad parts about humanity.
And in the end of the poem, three of them are twarthed off but there is still the fourth.
The judge.
Even if we thorugh some miracle manage to handle climate change, that doesn't change the fact that we betted on being saved by technology.

**Style:** Long narative form like *the wasteland*

#### TheTalk
**Story:** 
A dream like sequence

**Core:**
It's about writing
It's about he pressure of writing something good
and all the tricks one can fool one self with.
It talks about giving up, and to go on.
and what winning entails.


**Style:**
Starts of with iambic pentameter
-> Imabic tetrameter


#### I wish I could just die, I wish I was alive
**Story:**
A recollection of past events.

**Core:**
Depression and desperation.
Both wanting to live and die

**Style:**
ababbcbC x 3 + bcbC

#### The castle
**Story:** 
The castle is about to fall and we need to flee.

**Core:**
Our reality is crashing down and we need to construct a new one.
A better one.


**Style:** prose poetry + halfrhymed stuff for hades


#### The Villanelle
**Story:** 
About giving up what you have, what you belive to evolve.

**Core:**
About giving up what you have, what you belive to evolve.
letting go.


**Style:**
Villanelle

#### 20. I could not find the fault of my shapeless form
**Story:** 
The ending of the collection

**Core:** 
Refrencing earlier poems.
About creating meaning out of nothng.

**Style:** 
Shakespearean Sonnet

